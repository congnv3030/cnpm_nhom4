﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLQuanAn.DTO
{
    public class Account
    {
        private string userName;
        public string UserName { get => userName; set => userName = value; }

        private string passWord;
        public string PassWord { get => passWord; set => passWord = value; }

        private string displayName;
        public string DisplayName { get => displayName; set => displayName = value; }    

        private int type;
        public int Type { get => type; set => type = value; }     

        public Account(string username, string displayname, int type, string password = null)
        {
            this.UserName = username;
            this.PassWord = password;
            this.DisplayName = displayname;
            this.Type = type;
        }

        public Account(DataRow row)
        {
            this.UserName = row["UserName"].ToString();
            this.PassWord = row["PassWord"].ToString();
            this.DisplayName = row["DisplayName"].ToString();
            this.Type = (int)row["Type"];
        }
    }
}
