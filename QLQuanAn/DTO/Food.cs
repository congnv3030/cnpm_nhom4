﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLQuanAn.DTO
{
    public class Food
    {
        private int iD;
        public int ID { get => iD; set => iD = value; }

        private string foodName;
        public string FoodName { get => foodName; set => foodName = value; }        

        private int idCategory;
        public int IdCategory { get => idCategory; set => idCategory = value; }

        private float price;
        public float Price { get => price; set => price = value; }

        public Food(int id, string foodname, int idcategory, float price)
        {
            this.ID = id;
            this.FoodName = foodname;
            this.IdCategory = idcategory;
            this.Price = price;
        }

        public Food(DataRow row)
        {
            this.ID = (int)row["ID"];
            this.FoodName = row["FoodName"].ToString();
            this.IdCategory = (int)row["IdCategory"];
            this.Price = (float)(double)row["Price"];
        }
    }
}
