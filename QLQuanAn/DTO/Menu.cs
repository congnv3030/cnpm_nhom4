﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLQuanAn.DTO
{
    public class Menu
    {
        private string foodName;
        public string FoodName { get => foodName; set => foodName = value; }

        private int count;
        public int Count { get => count; set => count = value; }

        private float price;
        public float Price { get => price; set => price = value; }        

        private float totalPrice;
        public float TotalPrice { get => totalPrice; set => totalPrice = value; }

        public Menu(string foodname, int count, float price, float totalprice = 0)
        {
            this.FoodName = foodname;
            this.Count = count;
            this.Price = price;
            this.TotalPrice = totalprice;
        }

        public Menu(DataRow row)
        {
            this.FoodName = row["FoodName"].ToString();
            this.Count = (int)row["Count"];
            this.Price = (float)(double)row["Price"];
            this.TotalPrice = (float)(double)row["ToTalPrice"];
        }
    }
}
