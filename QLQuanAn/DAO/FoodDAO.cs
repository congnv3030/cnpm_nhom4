﻿using QLQuanAn.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLQuanAn.DAO
{
    public class FoodDAO
    {
        private static FoodDAO instance;
        public static FoodDAO Instance
        {
            get { if (FoodDAO.instance == null) FoodDAO.instance = new FoodDAO(); return FoodDAO.instance; }
            private set { FoodDAO.instance = value; }
        }

        private FoodDAO() { }

        public DataTable SearchFoodByName(string name)
        {
            string query = string.Format("SELECT f.[ID] , [FoodName] , [Name] , [Price] FROM dbo.[Food] AS f LEFT JOIN dbo.[FoodCategory] AS fl ON f.[IdCategory] = fl.[ID] AND dbo.fuConvertToUnsign1([FoodName]) LIKE N'%' + dbo.fuConvertToUnsign1(N'{0}') + '%'", name);

            return DataProvider.Instance.ExecuteQuery(query);
        }

        public List<Food> GetListFoodByIdCategory(int id)
        {
            List<Food> listFood = new List<Food>();

            string query = "SELECT * FROM dbo.[Food] WHERE [IdCategory] = " + id;

            DataTable data = DataProvider.Instance.ExecuteQuery(query);

            foreach (DataRow item in data.Rows)
                listFood.Add(new Food(item));

            return listFood;
        }

        public DataTable GetListFood()
        {
            string query = "SELECT f.[ID] , [FoodName] , [Name] , [Price] FROM dbo.[Food] AS f LEFT JOIN dbo.[FoodCategory] AS fl ON f.[IdCategory] = fl.[ID]";

            return DataProvider.Instance.ExecuteQuery(query);
        }

        public bool InsertFood(string name, int id, float price)
        {
            int result = DataProvider.Instance.ExecuteNonQuery("EXEC USP_InsertFood @foodname , @idcategory , @price", new object[] { name, id, price });

            return result > 0;
        }

        public bool UpdateFood(int id, string name, int idcategory, float price)
        {
            int result = DataProvider.Instance.ExecuteNonQuery("EXEC USP_UpdateFood @id , @foodname , @idcategory , @price", new object[] { id, name, idcategory, price });

            return result > 0;
        }

        public bool DeleteFood(int id)
        {
            BillInfoDAO.Instance.DeleteBillInfoByFoodID(id);

            string query = "DELETE dbo.[Food] WHERE [Id] = " + id;

            int result = DataProvider.Instance.ExecuteNonQuery(query);

            return result > 0;
        }
    }
}
