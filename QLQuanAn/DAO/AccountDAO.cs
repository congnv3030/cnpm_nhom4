﻿using QLQuanAn.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace QLQuanAn.DAO
{
    public class AccountDAO
    {
        private static AccountDAO instance;

        public static AccountDAO Instance
        {
            get { if (AccountDAO.instance == null) AccountDAO.instance = new AccountDAO(); return AccountDAO.instance; }
            private set { AccountDAO.instance = value; }
        }

        private AccountDAO() { }

        private string Security(string value)
        {
            byte[] tmp = ASCIIEncoding.ASCII.GetBytes(value);
            byte[] hashData = new MD5CryptoServiceProvider().ComputeHash(tmp);

            string hashPass = "";

            foreach (byte item in hashData)
                hashPass += item;

            return hashPass;
        }

        public bool Login(string username, string password)
        {
            string hashPass = Security(password);     

            //var list = hashData.ToString();
            //list.Reverse();

            string query = "EXEC USP_Login @username , @password";
            return DataProvider.Instance.ExecuteReader(query, new object[] { username, hashPass/*list*/ });
        }

        public bool UpdateAccount(string username, string displayname, string pass, string newpass)
        {
            string hashpass = Security(pass);
            string hashnewpass = Security(newpass);

            int result = DataProvider.Instance.ExecuteNonQuery("EXEC USP_UpdateAccount @username , @displayname , @password , @newpassword", new object[] { username, displayname, hashpass, hashnewpass });

            return result > 0;
        }

        public Account GetAccountByUserName(string username)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT * FROM dbo.[Account] WHERE [UserName] = '" + username + "'");

            foreach (DataRow item in data.Rows)
            {
                return new Account(item);
            }

            return null;
        }

        public DataTable GetListAccount()
        {
            return DataProvider.Instance.ExecuteQuery("SELECT ROW_NUMBER() OVER (ORDER BY a.[UserName]) AS [STT] , a.[UserName] , a.[DisplayName] , [at].[TypeName] FROM dbo.[Account] AS a LEFT JOIN dbo.[AccountType] AS [at] ON a.[Type] = [at].[ID]");
        }


        public bool InsertAccount(string name, string displayname, int type)
        {
            int result = DataProvider.Instance.ExecuteNonQuery("EXEC USP_InsertAccount @username , @displayname , @type", new object[] { name, displayname, type });

            return result > 0;
        }

        public bool UpdateAccountByName(string name, string displayname, int type)
        {
            int result = DataProvider.Instance.ExecuteNonQuery("EXEC USP_UpdateAccountByName @username , @displayname , @type", new object[] { name, displayname, type });

            return result > 0;
        }

        public bool DeleteAccount(string name)
        {
            string query = "DELETE dbo.[Account] WHERE [UserName] = '" + name + "'";

            int result = DataProvider.Instance.ExecuteNonQuery(query);

            return result > 0;
        }

        public bool RestPassWordAccount(string name)
        {
            string query = "EXEC USP_RestPassWordAccount @username";

            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { name });

            return result > 0;
        }
    }
}
