﻿using QLQuanAn.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLQuanAn.DAO
{
    public class CategoryDAO
    {
        private static CategoryDAO instance;

        public static CategoryDAO Instance
        {
            get { if (CategoryDAO.instance == null) CategoryDAO.instance = new CategoryDAO(); return CategoryDAO.instance; }
            private set { CategoryDAO.instance = value; }
        }

        private CategoryDAO() { }

        public List<Category> GetListCategory()
        {
            List<Category> listCategory = new List<Category>();

            string query = "SELECT * FROM dbo.[FoodCategory]";

            DataTable data = DataProvider.Instance.ExecuteQuery(query);

            foreach (DataRow item in data.Rows)
                listCategory.Add(new Category(item));

            return listCategory;
        }

        public Category GetCategoryByID(int id)
        {
            Category category = null;

            string query = "SELECT * FROM dbo.[FoodCategory] WHERE [ID] = " + id;

            DataTable data = DataProvider.Instance.ExecuteQuery(query);

            foreach (DataRow item in data.Rows)
                return new Category(item);

            return category;
        }

        public DataTable SearchCategoryByName(string name)
        {
            string query = string.Format("SELECT * FROM dbo.[FoodCategory] WHERE dbo.fuConvertToUnsign1([Name]) LIKE N'%' + dbo.fuConvertToUnsign1(N'{0}') + '%'", name);

            return DataProvider.Instance.ExecuteQuery(query);
        }

        public bool InsertCategory(string name)
        {
            int result = DataProvider.Instance.ExecuteNonQuery("EXEC USP_InsertFoodCategory @name", new object[] { name });

            return result > 0;
        }

        public bool UpdateCategory(int id, string name)
        {
            int result = DataProvider.Instance.ExecuteNonQuery("EXEC USP_UpdateFoodCategory @id , @name", new object[] { id, name });

            return result > 0;
        }

        public bool DeleteCategory(int id)
        {
            string query = "EXEC USP_DeleteFoodCategory @id";

            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { id });

            return result > 0;
        }
    }
}
