﻿using QLQuanAn.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLQuanAn.DAO
{
    public class MenuDAO
    {
        private static MenuDAO instance;
        public static MenuDAO Instance
        {
            get { if (MenuDAO.instance == null) MenuDAO.instance = new MenuDAO(); return MenuDAO.instance; }
            private set { MenuDAO.instance = value; }
        }

        private MenuDAO() { }

        public List<Menu> GetListMenuByIdTable(int id)
        {
            string query = "SELECT f.[FoodName], bi.[Count], f.[Price], f.[Price]*bi.[Count] AS TotalPrice FROM dbo.[BillInfo] AS bi LEFT JOIN dbo.[Bill] AS b ON bi.[IdBill] = b.[ID] LEFT JOIN dbo.[Food] AS f ON bi.[IdFood] = f.[ID] WHERE b.[Status] = 0 AND b.[IdTable] = " + id;
            List<Menu> listMenu = new List<Menu>();
            DataTable data = DataProvider.Instance.ExecuteQuery(query);

            foreach (DataRow item in data.Rows)
                listMenu.Add(new Menu(item));

            return listMenu;
        }
    }
}
