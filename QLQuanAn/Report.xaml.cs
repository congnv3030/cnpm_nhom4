﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QLQuanAn
{
    /// <summary>
    /// Interaction logic for Report.xaml
    /// </summary>
    public partial class Report : Window
    {
        public Report(DateTime checkin, DateTime checkout)
        {
            InitializeComponent();
            this.DateCheckIn = checkin;
            this.DateCheckOut = checkout;
             _reportViewer.Load += ReportViewer_Load;
        }

        private DateTime dateCheckIn;
        public DateTime DateCheckIn { get => dateCheckIn; set => dateCheckIn = value; }
        
        private DateTime dateCheckOut;
        public DateTime DateCheckOut { get => dateCheckOut; set => dateCheckOut = value; }

        private bool _isReportViewerLoaded;    

        private void ReportViewer_Load(object sender, EventArgs e)
        {
            if (!_isReportViewerLoaded)
            {
                Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();

                QLQuanAnDataSet dataset = new QLQuanAnDataSet();

                dataset.BeginInit();

                reportDataSource1.Name = "DataSet1";
                reportDataSource1.Value = dataset.USP_GetListBillByDateEn;
                this._reportViewer.LocalReport.DataSources.Add(reportDataSource1);
                this._reportViewer.LocalReport.ReportEmbeddedResource = "QLQuanAn.Report1.rdlc";

                dataset.EndInit();

                //fill data into adventureWorksDataSet
                QLQuanAnDataSetTableAdapters.USP_GetListBillByDateEnTableAdapter datasetAdapter = new QLQuanAnDataSetTableAdapters.USP_GetListBillByDateEnTableAdapter();

                datasetAdapter.ClearBeforeFill = true;
                datasetAdapter.Fill(dataset.USP_GetListBillByDateEn, DateCheckIn, DateCheckOut);

                _reportViewer.RefreshReport();

                _isReportViewerLoaded = true;
            }
        }   
    }
}
