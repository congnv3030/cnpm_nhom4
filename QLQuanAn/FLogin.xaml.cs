﻿using QLQuanAn.DAO;
using System.Windows;

namespace QLQuanAn
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            string username = txbUserName.Text;
            if (Login(username, txbPassWord.Password))
            {
                FTableManager f = new FTableManager(AccountDAO.Instance.GetAccountByUserName(username));
                this.Hide();
                this.ShowInTaskbar = false;
                f.ShowDialog();
                this.ShowInTaskbar = true;
                this.Topmost = true;
                this.Show();
                this.Topmost = false;
            }
            else Popup.LOGIN_FAIL();
        }

        private bool Login(string username, string password)
        {
            return AccountDAO.Instance.Login(username, password);
        }

        private void FLogin_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Popup.EXIT() != MessageBoxResult.Yes) e.Cancel = true;
        }
    }
}
