USE [master]
GO
/****** Object:  Database [QLQuanAn]    Script Date: 14/06/2020 11:53:20 SA ******/
CREATE DATABASE [QLQuanAn]
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLQuanAn].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLQuanAn] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QLQuanAn] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QLQuanAn] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QLQuanAn] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QLQuanAn] SET ARITHABORT OFF 
GO
ALTER DATABASE [QLQuanAn] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QLQuanAn] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QLQuanAn] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QLQuanAn] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QLQuanAn] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QLQuanAn] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QLQuanAn] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QLQuanAn] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QLQuanAn] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QLQuanAn] SET  ENABLE_BROKER 
GO
ALTER DATABASE [QLQuanAn] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QLQuanAn] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QLQuanAn] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QLQuanAn] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QLQuanAn] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QLQuanAn] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QLQuanAn] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QLQuanAn] SET RECOVERY FULL 
GO
ALTER DATABASE [QLQuanAn] SET  MULTI_USER 
GO
ALTER DATABASE [QLQuanAn] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QLQuanAn] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QLQuanAn] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QLQuanAn] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [QLQuanAn] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'QLQuanAn', N'ON'
GO
ALTER DATABASE [QLQuanAn] SET QUERY_STORE = OFF
GO
USE [QLQuanAn]
GO
/****** Object:  UserDefinedFunction [dbo].[fuConvertToUnsign1]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fuConvertToUnsign1] ( @strInput NVARCHAR(4000) ) RETURNS NVARCHAR(4000) AS BEGIN IF @strInput IS NULL RETURN @strInput IF @strInput = '' RETURN @strInput DECLARE @RT NVARCHAR(4000) DECLARE @SIGN_CHARS NCHAR(136) DECLARE @UNSIGN_CHARS NCHAR (136) SET @SIGN_CHARS = N'ăâđêôơưàảãạáằẳẵặắầẩẫậấèẻẽẹéềểễệế ìỉĩịíòỏõọóồổỗộốờởỡợớùủũụúừửữựứỳỷỹỵý ĂÂĐÊÔƠƯÀẢÃẠÁẰẲẴẶẮẦẨẪẬẤÈẺẼẸÉỀỂỄỆẾÌỈĨỊÍ ÒỎÕỌÓỒỔỖỘỐỜỞỠỢỚÙỦŨỤÚỪỬỮỰỨỲỶỸỴÝ' +NCHAR(272)+ NCHAR(208) SET @UNSIGN_CHARS = N'aadeoouaaaaaaaaaaaaaaaeeeeeeeeee iiiiiooooooooooooooouuuuuuuuuuyyyyy AADEOOUAAAAAAAAAAAAAAAEEEEEEEEEEIIIII OOOOOOOOOOOOOOOUUUUUUUUUUYYYYYDD' DECLARE @COUNTER int DECLARE @COUNTER1 int SET @COUNTER = 1 WHILE (@COUNTER <=LEN(@strInput)) BEGIN SET @COUNTER1 = 1 WHILE (@COUNTER1 <=LEN(@SIGN_CHARS)+1) BEGIN IF UNICODE(SUBSTRING(@SIGN_CHARS, @COUNTER1,1)) = UNICODE(SUBSTRING(@strInput,@COUNTER ,1) ) BEGIN IF @COUNTER=1 SET @strInput = SUBSTRING(@UNSIGN_CHARS, @COUNTER1,1) + SUBSTRING(@strInput, @COUNTER+1,LEN(@strInput)-1) ELSE SET @strInput = SUBSTRING(@strInput, 1, @COUNTER-1) +SUBSTRING(@UNSIGN_CHARS, @COUNTER1,1) + SUBSTRING(@strInput, @COUNTER+1,LEN(@strInput)- @COUNTER) BREAK END SET @COUNTER1 = @COUNTER1 +1 END SET @COUNTER = @COUNTER +1 END SET @strInput = replace(@strInput,' ','-') RETURN @strInput END
GO
/****** Object:  Table [dbo].[Account]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[UserName] [nvarchar](100) NOT NULL,
	[PassWord] [nvarchar](max) NOT NULL,
	[DisplayName] [nvarchar](100) NOT NULL,
	[Type] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountType]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bill]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateCheckIn] [date] NOT NULL,
	[DateCheckOut] [date] NULL,
	[IdTable] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[Discount] [int] NULL,
	[TotalPrice] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BillInfo]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IdBill] [int] NOT NULL,
	[IdFood] [int] NOT NULL,
	[Count] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Food]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Food](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FoodName] [nvarchar](100) NOT NULL,
	[IdCategory] [int] NOT NULL,
	[Price] [float] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FoodCategory]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FoodCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Table]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Status] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Account] ([UserName], [PassWord], [DisplayName], [Type]) VALUES (N'admin', N'33354741122871651676713774147412831195', N'Nguyễn Văn Công', 1)
INSERT [dbo].[Account] ([UserName], [PassWord], [DisplayName], [Type]) VALUES (N'guest', N'guest', N'Người dùng thử nghiệm', 2)
SET IDENTITY_INSERT [dbo].[AccountType] ON 

INSERT [dbo].[AccountType] ([ID], [TypeName]) VALUES (1, N'Người quản lý')
INSERT [dbo].[AccountType] ([ID], [TypeName]) VALUES (2, N'Nhân viên')
SET IDENTITY_INSERT [dbo].[AccountType] OFF
SET IDENTITY_INSERT [dbo].[Bill] ON 

INSERT [dbo].[Bill] ([ID], [DateCheckIn], [DateCheckOut], [IdTable], [Status], [Discount], [TotalPrice]) VALUES (7, CAST(N'2020-06-04' AS Date), CAST(N'2020-06-04' AS Date), 1, 1, 0, 150000)
INSERT [dbo].[Bill] ([ID], [DateCheckIn], [DateCheckOut], [IdTable], [Status], [Discount], [TotalPrice]) VALUES (8, CAST(N'2020-06-04' AS Date), CAST(N'2020-06-04' AS Date), 2, 1, 0, 150000)
INSERT [dbo].[Bill] ([ID], [DateCheckIn], [DateCheckOut], [IdTable], [Status], [Discount], [TotalPrice]) VALUES (9, CAST(N'2020-06-04' AS Date), CAST(N'2020-06-04' AS Date), 5, 1, 0, 150000)
INSERT [dbo].[Bill] ([ID], [DateCheckIn], [DateCheckOut], [IdTable], [Status], [Discount], [TotalPrice]) VALUES (10, CAST(N'2020-06-04' AS Date), CAST(N'2020-06-04' AS Date), 9, 1, 10, 135000)
INSERT [dbo].[Bill] ([ID], [DateCheckIn], [DateCheckOut], [IdTable], [Status], [Discount], [TotalPrice]) VALUES (11, CAST(N'2020-06-04' AS Date), CAST(N'2020-06-04' AS Date), 6, 1, 10, 90000)
INSERT [dbo].[Bill] ([ID], [DateCheckIn], [DateCheckOut], [IdTable], [Status], [Discount], [TotalPrice]) VALUES (12, CAST(N'2020-06-04' AS Date), CAST(N'2020-06-04' AS Date), 9, 1, 5, 142500)
INSERT [dbo].[Bill] ([ID], [DateCheckIn], [DateCheckOut], [IdTable], [Status], [Discount], [TotalPrice]) VALUES (13, CAST(N'2020-06-04' AS Date), CAST(N'2020-06-04' AS Date), 4, 1, 0, 50000)
INSERT [dbo].[Bill] ([ID], [DateCheckIn], [DateCheckOut], [IdTable], [Status], [Discount], [TotalPrice]) VALUES (14, CAST(N'2020-06-05' AS Date), NULL, 2, 0, 0, NULL)
INSERT [dbo].[Bill] ([ID], [DateCheckIn], [DateCheckOut], [IdTable], [Status], [Discount], [TotalPrice]) VALUES (15, CAST(N'2020-06-06' AS Date), NULL, 6, 0, 0, NULL)
SET IDENTITY_INSERT [dbo].[Bill] OFF
SET IDENTITY_INSERT [dbo].[BillInfo] ON 

INSERT [dbo].[BillInfo] ([ID], [IdBill], [IdFood], [Count]) VALUES (1, 7, 1, 11)
INSERT [dbo].[BillInfo] ([ID], [IdBill], [IdFood], [Count]) VALUES (2, 8, 1, 11)
INSERT [dbo].[BillInfo] ([ID], [IdBill], [IdFood], [Count]) VALUES (3, 9, 1, 11)
INSERT [dbo].[BillInfo] ([ID], [IdBill], [IdFood], [Count]) VALUES (4, 10, 1, 11)
INSERT [dbo].[BillInfo] ([ID], [IdBill], [IdFood], [Count]) VALUES (5, 11, 1, 11)
INSERT [dbo].[BillInfo] ([ID], [IdBill], [IdFood], [Count]) VALUES (6, 12, 1, 11)
INSERT [dbo].[BillInfo] ([ID], [IdBill], [IdFood], [Count]) VALUES (7, 13, 1, 11)
INSERT [dbo].[BillInfo] ([ID], [IdBill], [IdFood], [Count]) VALUES (8, 14, 1, 11)
INSERT [dbo].[BillInfo] ([ID], [IdBill], [IdFood], [Count]) VALUES (9, 15, 1, 11)
INSERT [dbo].[BillInfo] ([ID], [IdBill], [IdFood], [Count]) VALUES (10, 14, 2, 1)
SET IDENTITY_INSERT [dbo].[BillInfo] OFF
SET IDENTITY_INSERT [dbo].[Food] ON 

INSERT [dbo].[Food] ([ID], [FoodName], [IdCategory], [Price]) VALUES (1, N'Mực khô nướng', 1, 50000)
INSERT [dbo].[Food] ([ID], [FoodName], [IdCategory], [Price]) VALUES (2, N'Cá khô nướng', 1, 30000)
INSERT [dbo].[Food] ([ID], [FoodName], [IdCategory], [Price]) VALUES (3, N'Khô gà lá chanh', 1, 20000)
INSERT [dbo].[Food] ([ID], [FoodName], [IdCategory], [Price]) VALUES (4, N'Khô bò', 1, 10000)
INSERT [dbo].[Food] ([ID], [FoodName], [IdCategory], [Price]) VALUES (5, N'Cafe đá', 2, 10000)
INSERT [dbo].[Food] ([ID], [FoodName], [IdCategory], [Price]) VALUES (6, N'Cafe sữa đá', 2, 15000)
INSERT [dbo].[Food] ([ID], [FoodName], [IdCategory], [Price]) VALUES (7, N'Bia', 2, 5000)
INSERT [dbo].[Food] ([ID], [FoodName], [IdCategory], [Price]) VALUES (8, N'7UP', 2, 10000)
INSERT [dbo].[Food] ([ID], [FoodName], [IdCategory], [Price]) VALUES (9, N'Pepsi', 2, 10000)
INSERT [dbo].[Food] ([ID], [FoodName], [IdCategory], [Price]) VALUES (10, N'Khăn giấy', 3, 5000)
INSERT [dbo].[Food] ([ID], [FoodName], [IdCategory], [Price]) VALUES (11, N'Đậu phộng', 3, 5000)
SET IDENTITY_INSERT [dbo].[Food] OFF
SET IDENTITY_INSERT [dbo].[FoodCategory] ON 

INSERT [dbo].[FoodCategory] ([ID], [Name]) VALUES (1, N'Đồ khô')
INSERT [dbo].[FoodCategory] ([ID], [Name]) VALUES (2, N'Giải khát')
INSERT [dbo].[FoodCategory] ([ID], [Name]) VALUES (3, N'Lặt vặt')
SET IDENTITY_INSERT [dbo].[FoodCategory] OFF
SET IDENTITY_INSERT [dbo].[Table] ON 

INSERT [dbo].[Table] ([ID], [Name], [Status]) VALUES (1, N'Bàn 1', N'Trống')
INSERT [dbo].[Table] ([ID], [Name], [Status]) VALUES (2, N'Bàn 2', N'Đã có người')
INSERT [dbo].[Table] ([ID], [Name], [Status]) VALUES (3, N'Bàn 3', N'Trống')
INSERT [dbo].[Table] ([ID], [Name], [Status]) VALUES (4, N'Bàn 4', N'Trống')
INSERT [dbo].[Table] ([ID], [Name], [Status]) VALUES (5, N'Bàn 5', N'Trống')
INSERT [dbo].[Table] ([ID], [Name], [Status]) VALUES (6, N'Bàn 6', N'Đã có người')
INSERT [dbo].[Table] ([ID], [Name], [Status]) VALUES (7, N'Bàn 7', N'Trống')
INSERT [dbo].[Table] ([ID], [Name], [Status]) VALUES (8, N'Bàn 8', N'Trống')
INSERT [dbo].[Table] ([ID], [Name], [Status]) VALUES (9, N'Bàn 9', N'Trống')
INSERT [dbo].[Table] ([ID], [Name], [Status]) VALUES (10, N'Bàn 10', N'Trống')
SET IDENTITY_INSERT [dbo].[Table] OFF
ALTER TABLE [dbo].[Account] ADD  DEFAULT ((0)) FOR [PassWord]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT (N'Chưa đặt tên') FOR [DisplayName]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [dbo].[AccountType] ADD  DEFAULT (N'Nhân viên') FOR [TypeName]
GO
ALTER TABLE [dbo].[Bill] ADD  DEFAULT (getdate()) FOR [DateCheckIn]
GO
ALTER TABLE [dbo].[Bill] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[Bill] ADD  DEFAULT ((0)) FOR [Discount]
GO
ALTER TABLE [dbo].[BillInfo] ADD  DEFAULT ((0)) FOR [Count]
GO
ALTER TABLE [dbo].[Food] ADD  DEFAULT (N'Chưa đặt tên') FOR [FoodName]
GO
ALTER TABLE [dbo].[Food] ADD  DEFAULT ((0)) FOR [Price]
GO
ALTER TABLE [dbo].[FoodCategory] ADD  DEFAULT (N'Chưa đặt tên') FOR [Name]
GO
ALTER TABLE [dbo].[Table] ADD  DEFAULT (N'Bàn chưa đặt tên') FOR [Name]
GO
ALTER TABLE [dbo].[Table] ADD  DEFAULT (N'Trống') FOR [Status]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD FOREIGN KEY([Type])
REFERENCES [dbo].[AccountType] ([ID])
GO
ALTER TABLE [dbo].[Bill]  WITH CHECK ADD FOREIGN KEY([IdTable])
REFERENCES [dbo].[Table] ([ID])
GO
ALTER TABLE [dbo].[BillInfo]  WITH CHECK ADD FOREIGN KEY([IdBill])
REFERENCES [dbo].[Bill] ([ID])
GO
ALTER TABLE [dbo].[BillInfo]  WITH CHECK ADD FOREIGN KEY([IdFood])
REFERENCES [dbo].[Food] ([ID])
GO
ALTER TABLE [dbo].[Food]  WITH CHECK ADD FOREIGN KEY([IdCategory])
REFERENCES [dbo].[FoodCategory] ([ID])
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteFoodCategory]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_DeleteFoodCategory]
@id INT
AS
BEGIN

	SELECT [ID] INTO [IdFoodTmp] FROM dbo.[Food] WHERE [IdCategory] = @id

	DELETE dbo.[BillInfo] WHERE [IdFood] IN (SELECT * FROM [IdFoodTmp])

	DROP TABLE [IdFoodTmp]

	DELETE dbo.[Food] WHERE [IdCategory] = @id

	DELETE dbo.[FoodCategory] WHERE [ID] = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteTable]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_DeleteTable]
@id INT
AS
BEGIN
	
	SELECT [ID] INTO [IdBillTmp] FROM dbo.[Bill] WHERE [IdTable] = @id

	DELETE dbo.[BillInfo] WHERE [IdBill] IN (SELECT * FROM [IdBillTmp])

	DROP TABLE [IdBillTmp]

	DELETE dbo.[Bill] WHERE [IdTable] = @id

	DELETE dbo.[Table] WHERE [ID] = @id

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAccountByUserName]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_GetAccountByUserName]
@username NVARCHAR(100)
AS 
BEGIN
	SELECT * FROM dbo.[Account] WHERE [UserName] = @username
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetListBillByDate]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_GetListBillByDate]
@datecheckin DATE, @datacheckout DATE
AS
BEGIN
	SELECT t.[Name] AS [Tên bàn ăn], b.[TotalPrice] AS [Tổng thanh toán (VNĐ)], b.[DateCheckIn] AS [Ngày tạo], b.[DateCheckOut] AS [Ngày thanh toán], b.[Discount] AS [Giảm giá (%)] 
	FROM dbo.[Bill] AS b
	JOIN dbo.[Table] AS t ON b.[IdTable] = t.[ID]
	WHERE b.[Status] = 1 AND [DateCheckIn] >= @datecheckin AND [DateCheckOut] <= @datacheckout
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetListBillByDateAndPage]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_GetListBillByDateAndPage]
@datecheckin DATE, @datacheckout DATE, @page INT
AS
BEGIN

	DECLARE @pagerows INT = 5
	DECLARE @select INT = @pagerows 
	DECLARE @except INT = (@page - 1) * @pagerows

	;WITH [BillShow] AS (SELECT b.[ID], t.[Name] AS [Tên bàn ăn], b.[TotalPrice] AS [Tổng thanh toán (VNĐ)], b.[DateCheckIn] AS [Ngày tạo], b.[DateCheckOut] AS [Ngày thanh toán], b.[Discount] AS [Giảm giá (%)] 
	FROM dbo.[Bill] AS b
	JOIN dbo.[Table] AS t ON t.[ID] = b.[IdTable]
	WHERE b.[Status] = 1 AND [DateCheckIn] >= @datecheckin AND [DateCheckOut] <= @datacheckout)

	SELECT TOP (@select) * FROM [BillShow] WHERE [ID] NOT IN (SELECT TOP (@except) [ID] FROM [BillShow])

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetListBillByDateEn]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_GetListBillByDateEn]
@datecheckin DATE, @datacheckout DATE
AS
BEGIN
	SELECT t.[Name] , b.[TotalPrice] , b.[DateCheckIn] , b.[DateCheckOut] , b.[Discount] FROM dbo.[Bill] AS b
	JOIN dbo.[Table] AS t ON b.[IdTable] = t.[ID]
	WHERE b.[Status] = 1 AND [DateCheckIn] >= @datecheckin AND [DateCheckOut] <= @datacheckout
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetMaxPageBillByDate]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_GetMaxPageBillByDate]
@datecheckin DATE, @datacheckout DATE
AS
BEGIN
	SELECT COUNT(*)
	FROM dbo.[Bill] AS b
	JOIN dbo.[Table] AS t ON b.[IdTable] = t.[ID]
	WHERE b.[Status] = 1 AND [DateCheckIn] >= @datecheckin AND [DateCheckOut] <= @datacheckout
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetTableList]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_GetTableList]
AS SELECT * FROM dbo.[Table]
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertAccount]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_InsertAccount]
@username NVARCHAR(100) , @displayname NVARCHAR(100) , @type INT
AS
BEGIN
	INSERT INTO dbo.[Account]
        ( [UserName] ,
			[PassWord],
          [DisplayName] ,
		  [Type] 
        )
	VALUES  ( @username , -- UserName - nvarchar(100)
			N'20720532132149213101239102231223249249135100218',
          @displayname , -- DisplayName - nvarchar(100)
		  @type
        )
END
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertBill]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_InsertBill]
@idtable INT
AS 
BEGIN
	INSERT	dbo.[Bill]
        ( [DateCheckOut] ,
          [IdTable] 
        )
	VALUES  ( NULL , -- DateCheckOut - date
          @idtable  -- idTable - int
        )
END
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertBillInfo]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_InsertBillInfo]
@idbill INT, @idfood INT, @count INT
AS
BEGIN

	DECLARE @isExitBillInfo INT 
	DECLARE @foodcount INT = 0

	SELECT @isExitBillInfo = b.[ID], @foodcount = b.[Count] 
	FROM dbo.[BillInfo] AS b
	WHERE b.[IdBill] = @idbill AND b.[IdFood] = @idfood

	IF(@isExitBillInfo > 0)
	BEGIN
		DECLARE @newcount INT = @foodcount + @count
		IF(@newcount > 0) UPDATE dbo.[BillInfo] SET [Count] = @newcount WHERE [IdFood] = @idfood
		ELSE DELETE FROM dbo.[BillInfo] WHERE [IdBill] = @idbill AND [IdFood] = @idfood
	END
	ELSE
	BEGIN
		INSERT	dbo.[BillInfo]
        ( [IdBill], [IdFood], [Count] )
		VALUES  ( @idbill, -- idBill - int
          @idfood, -- idFood - int
          @count  -- count - int
          )
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertFood]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_InsertFood]
@foodname NVARCHAR(100), @idcategory INT, @price FLOAT
AS
BEGIN
	INSERT dbo.[Food] ( [FoodName], [IdCategory], [Price] )
	VALUES  ( @foodname, @idcategory, @price)
END
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertFoodCategory]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_InsertFoodCategory]
@name NVARCHAR(100)
AS 
BEGIN
	INSERT dbo.[FoodCategory]
        ( [Name] )
	VALUES  ( @name )
END
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertTable]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_InsertTable]
@name NVARCHAR(100)
AS
BEGIN
	INSERT dbo.[Table] ( [Name] ) VALUES  ( @name )
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Login]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_Login]
@username NVARCHAR(100), @password NVARCHAR(MAX)
AS
BEGIN
	SELECT * FROM dbo.[Account] WHERE [UserName] = @username COLLATE SQL_Latin1_General_CP1_CS_AS 
	AND [PassWord] = @password COLLATE SQL_Latin1_General_CP1_CS_AS 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_RestPassWordAccount]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_RestPassWordAccount]
@username NVARCHAR(100)
AS 
BEGIN
	UPDATE dbo.[Account] SET [PassWord] = N'20720532132149213101239102231223249249135100218' WHERE [UserName] = @username
END
GO
/****** Object:  StoredProcedure [dbo].[USP_SwitchTable]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_SwitchTable]
@idtable1 INT, @idtable2 INT
AS
BEGIN

	DECLARE @idbill1 INT

	DECLARE @idbill2 INT

	DECLARE @isFirstTablEmty INT = 1

	DECLARE @isSecondTablEmty INT = 1

	SELECT @idbill1 = [ID] FROM dbo.[Bill] WHERE [IdTable] = @idtable1 AND [Status] = 0 

	SELECT @idbill2 = [ID] FROM dbo.[Bill] WHERE [IdTable] = @idtable2 AND [Status] = 0 

	IF(@idbill1 IS NULL)
	BEGIN
		INSERT	dbo.[Bill]
        ( [DateCheckOut] ,
          [IdTable] 
        )
		VALUES  ( NULL , -- DateCheckOut - date
          @idtable1  -- idTable - int
        )

		SELECT @idbill1 = MAX([ID]) FROM dbo.[Bill] WHERE [IdTable] = @idtable1 AND [Status] = 0
	END

	SELECT @isFirstTablEmty = COUNT(*) FROM dbo.[BillInfo] WHERE [IdBill] = @idbill1

	IF(@idbill2 IS NULL)
	BEGIN
		INSERT	dbo.[Bill]
        ( [DateCheckOut] ,
          [IdTable] 
        )
		VALUES  ( NULL , -- DateCheckOut - date
          @idtable2  -- idTable - int
        )

		SELECT @idbill2 = MAX([ID]) FROM dbo.[Bill] WHERE [IdTable] = @idtable2 AND [Status] = 0
	END

	SELECT @isSecondTablEmty = COUNT(*) FROM dbo.[BillInfo] WHERE [IdBill] = @idbill2

	SELECT [ID] INTO [IdBillInfoTable] FROM dbo.[BillInfo] WHERE [IdBill] = @idbill2

	UPDATE dbo.[BillInfo] SET [IdBill] = @idbill2 WHERE [IdBill] = @idbill1

	UPDATE dbo.[BillInfo] SET [IdBill] = @idbill1 WHERE	[ID] IN (SELECT * FROM [IdBillInfoTable])

	DROP TABLE [IdBillInfoTable]

	IF (@isFirstTablEmty = 0)
		UPDATE dbo.[Table] SET [Status] = DEFAULT WHERE id = @idtable2
		
	IF (@isSecondTablEmty= 0)
		UPDATE dbo.[Table] SET [Status] = DEFAULT WHERE id = @idtable1
	
END
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateAccount]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_UpdateAccount]
@username NVARCHAR(100), @displayname NVARCHAR(100), @password NVARCHAR(MAX), @newpassword NVARCHAR(MAX)
AS
BEGIN
	DECLARE @isrightpass INT = 0

	SELECT @isrightpass = COUNT(*) FROM dbo.[Account] WHERE [UserName] = @username AND [PassWord] = @password

	IF(@isrightpass = 1)
	BEGIN

		IF(@newpassword = NULL OR @newpassword = '')
		BEGIN
			UPDATE dbo.[Account] SET [DisplayName] = @displayname WHERE [UserName] = @username
		END
		ELSE
			UPDATE dbo.[Account] SET [DisplayName] = @displayname, [PassWord] = @newpassword WHERE [UserName] = @username
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateAccountByName]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_UpdateAccountByName]
@username NVARCHAR(100) , @displayname NVARCHAR(100) , @type INT
AS 
BEGIN
	UPDATE dbo.[Account] SET [DisplayName] = @displayname , [Type] = @type WHERE [UserName] = @username
END
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateFood]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_UpdateFood]
@id INT, @foodname NVARCHAR(100), @idcategory INT, @price FLOAT
AS
BEGIN
	UPDATE dbo.[Food] SET [FoodName] = @foodname , [IdCategory] = @idcategory , [Price] = @price WHERE [ID] = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateFoodCategory]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_UpdateFoodCategory]
@id INT , @name NVARCHAR(100)
AS
BEGIN
	UPDATE dbo.[FoodCategory] SET [Name] = @name WHERE [ID] = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateTable]    Script Date: 14/06/2020 11:53:20 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_UpdateTable]
@id INT , @name NVARCHAR(100)
AS
BEGIN
	UPDATE dbo.[Table] SET [Name] = @name WHERE [ID] = @id
END
GO

CREATE TRIGGER UTG_UpdateBillInfo
ON dbo.[BillInfo] FOR INSERT, UPDATE
AS
BEGIN
	DECLARE @idbill INT

	SELECT @idbill = [IdBill] FROM Inserted

	DECLARE @idtable INT

	SELECT @idtable = [IdTable] FROM dbo.[Bill] WHERE [ID] = @idbill AND [Status] = 0

	DECLARE @count INT

	SELECT @count = COUNT(*) FROM dbo.[BillInfo] WHERE [IdBill] = @idbill

	IF (@count > 0)
		UPDATE dbo.[Table] SET [Status] = N'Đã có người' WHERE [ID] = @idtable
	ELSE
		UPDATE dbo.[Table] SET [Status] = DEFAULT WHERE [ID] = @idtable	
END
GO

CREATE TRIGGER UTG_UpdateBill
ON dbo.[Bill] FOR UPDATE
AS
BEGIN
	DECLARE @idbill INT

	SELECT @idbill = [ID] FROM Inserted

	DECLARE @idtable INT

	SELECT @idtable = [IdTable] FROM dbo.[Bill] WHERE [ID] = @idbill

	DECLARE @count INT = 0

	SELECT @count = COUNT(*) FROM dbo.[Bill] WHERE [IdTable] = @idtable AND [Status] = 0

	IF(@count = 0) UPDATE dbo.[Table] SET [Status] = DEFAULT WHERE [ID] = @idtable
END
GO

CREATE TRIGGER UTG_DeleteBillInfo
ON dbo.[BillInfo] FOR DELETE
AS 
BEGIN
	DECLARE @idbillinfo INT
	DECLARE @idbill INT

	SELECT @idbillinfo = [ID] , @idbill = Deleted.[IdBill] FROM Deleted

	DECLARE @idtable INT
	SELECT @idtable = [IdTable] FROM dbo.[Bill] WHERE [ID] = @idbill

	DECLARE @count INT = 0

	SELECT @count = COUNT(*) FROM dbo.[BillInfo] , dbo.[Bill] as b WHERE b.[ID] = [IdBill] AND b.[ID] = @idbill AND B.[Status] = 0

	IF(@count = 0)
	BEGIN
		UPDATE dbo.[Table] SET [Status] = DEFAULT WHERE [ID] = @idtable
		DELETE dbo.[Bill] WHERE [ID] = @idbill
	END
END
GO

USE [master]
GO
ALTER DATABASE [QLQuanAn] SET  READ_WRITE 
GO
